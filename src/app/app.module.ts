/** Angular Modules */
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {HttpClientModule} from '@angular/common/http';
import {FormsModule} from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms'; 
/** NGX BOOSTRAP MODULE */
import { TooltipModule } from 'ngx-bootstrap/tooltip';
import { CarouselModule } from 'ngx-bootstrap/carousel';

/**Routes */
import {appRoutes} from './routes';
import {RouterModule} from '@angular/router';


/**Services */
import {SearchService} from './services/search.service';
import {SeriesService} from './services/series.service';
import {AuthService} from './services/auth.service';
import { NewsService } from './services/news.service';

import { AppComponent } from './app.component';
import { NavBarComponent } from './nav-bar/nav-bar.component';
import { MyseriesComponent } from './myseries/myseries.component';
import { MyseriesViewComponent } from './myseries-view/myseries-view.component';
import { AuthComponent } from './auth/auth.component';
import { SearchComponent } from './search/search.component';
import { Error404Component } from './error404/error404.component';
import { NewsComponent } from './news/news.component';
import { NewsViewComponent } from './news-view/news-view.component';
import { SerieComponent } from './serie/serie.component';
import { HomeViewComponent } from './home-view/home-view.component';
import { SearchResultComponent } from './search-result/search-result.component';
import { SearchResultItemComponent } from './search-result-item/search-result-item.component';


@NgModule({
  declarations: [
    AppComponent,
    NavBarComponent,
    MyseriesComponent,
    MyseriesViewComponent,
    AuthComponent,
    SearchComponent,
    Error404Component,
    NewsComponent,
    NewsViewComponent,
    SerieComponent,
    HomeViewComponent,
    SearchResultComponent,
    SearchResultItemComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    RouterModule.forRoot(appRoutes),
    BrowserModule,
    TooltipModule.forRoot(),
    CarouselModule.forRoot(),
    ReactiveFormsModule
  ],
  providers: [
    AuthService,
    SeriesService,
    SearchService,
    NewsService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
