import { Component, OnInit , Input } from '@angular/core';

@Component({
  selector: 'app-search-result-item',
  templateUrl: './search-result-item.component.html',
  styleUrls: ['./search-result-item.component.scss']
})
export class SearchResultItemComponent implements OnInit {
  @Input() name: string;
  @Input () id: number;
  @Input () image: string;
  @Input() network: string;
  @Input() genre: Array<any>;
  @Input() webchannel: Array<any>;
  @Input () premiered: Date;
  constructor() { }

  ngOnInit() {
  }

}
