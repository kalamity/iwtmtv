import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class SearchService {
  result = [];
  public searchSubject: Subject<any[]>;


  constructor(private httpClient: HttpClient) {
    this.searchSubject = new Subject<any[]>();
  }

  emitSearch() {
    this.searchSubject.next(this.result); // mise a jour de l'annonce dans tout les composant
  }
  /*Récupérer les résultat de la recherche*/

getResult(result){
  return this.httpClient.get(`http://api.tvmaze.com/search/shows?q=${result.search}`).subscribe(
    data =>{
      let results = [];
      let r = new Map();
      for ( let [id, result]of Object.entries(data)){
        result.id = id
        results.push(result)
      }
       this.result = results
        this.emitSearch();
        console.log(results);
    }
  );
}

}
