import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import { News } from '../interfaces/news';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class NewsService {
  news: News[] = [];
  public newsSubject: Subject<News[]>;
  private readonly api: string = `https://watch-to-much-tv.firebaseio.com/`;

  constructor(private httpClient: HttpClient) {
    this.newsSubject = new Subject<News[]>();
  }

  emitNews() {
    this.newsSubject.next(this.news); // mise a jour de l'annonce dans tout les composant
  }
/** Récupéter la listes des News */
  getNews() {
    this.httpClient.get<News[]>(`${this.api}/news.json`).subscribe(
      data => {
        const news = [];
        for (let [id, aNew] of Object.entries(data)){
          aNew.id= id;
          news.push(aNew);
        }
        this.news = news;
        this.emitNews();
      },
      error => {
        console.error(error);
      },
      () => {
        console.info('All the Series are retrived');
      }
    );
  }
}
