import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Series } from '../interfaces/series';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SeriesService {
  series: Series[] = [];
  public seriesSubject: Subject<Series[]>;
  private readonly api: string = `https://watch-to-much-tv.firebaseio.com/`;

  constructor(private httpClient: HttpClient) {
    this.seriesSubject = new Subject<Series[]>();
  }
  emitSeries() {
    this.seriesSubject.next(this.series); // mise a jour de l'annonce dans tout les composant
  }

  /** Récupéter la listes des Series */
  getSeries() {
    this.httpClient.get<Series[]>(`${this.api}/series.json`).subscribe(
      data => {
        let series = [];
        let s = new Map();
        for (let [id, serie] of Object.entries(data)) {
          serie.id = id;
          series.push(serie);
        }
        this.series = series;
        this.emitSeries();
      },
      error => {
        console.error(error);
      },
      () => {
        console.info('All the Series are retrived');
      }
    );
  }

// /***Add a Serie***/
// public addSerie(serie) {
//   return this.httpClient.serie<{ title }>(`${this.api}/series.json`, serie).subscribe(
//     data => {
//       serie.id = data.name;
//       this.series.push(serie);
//       this.emitSeries();

//     },
//     error => console.error('SeriesService.addSerie(): ', error),
//     () => console.info('SeriesService.addSerie(): complete')
//   );
// }

/***Delete a Serie***/

public deleteSerie(serie) {

  return this.httpClient.delete<Series[]>(`${this.api}/series/${serie.id}.json`, serie).subscribe( //suprime en base de donnée
    data => {
      let index = this.series.findIndex(p => p.id == serie.id); //cherchea serie
      this.series.splice(index, 1); //supprime
      this.emitSeries();
    },
    error => console.error('SerieService.deleteSerie():', error),
    () => console.info('SerieService.deleteSerie(): Fini')
  );
}


/***Patch a Episode of a Serie***/

private _findById(id) {
  return this.series.find((serie) => serie.id == id);
}

episodes(id) {
  let serie = this._findById(id);
  serie.seasonV += 1;
  let patch = { seasonV : serie.seasonV};
  return this.httpClient.patch<Series[]>(`${this.api}/series/${serie.id}.json`, patch).subscribe(
    data =>{
      this.emitSeries();
    },
    error => console.error('SerieService.updateserie():', error),
    () => console.info('SerieService.updateserie():')
  )
}
}
