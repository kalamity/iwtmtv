import {Component, OnInit, Input} from '@angular/core';

@Component({
  selector: 'app-myseries',
  templateUrl: './myseries.component.html',
  styleUrls: ['./myseries.component.scss'],
})
export class MyseriesComponent implements OnInit {
  @Input()id: string;
  @Input () title: string;
  @Input () network: string;
  @Input () country: string;
  @Input () date: Date;
  @Input () season: number;
  @Input () seasonV: number;
  @Input() summary: string;
  @Input () rating: number;
  @Input () genres: Array<any>;
  @Input () image: string;


  ngOnInit() {

  }



}
