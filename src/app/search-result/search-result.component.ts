import { Component, OnInit } from '@angular/core';
import { SearchService } from '../services/search.service';

@Component({
  selector: 'app-search-result',
  templateUrl: './search-result.component.html',
  styleUrls: ['./search-result.component.scss']
})
export class SearchResultComponent implements OnInit {

  results = [];


  constructor(private searchService: SearchService) { }

  ngOnInit() {
    this.searchService.searchSubject.subscribe((results) => {
      this.results = results;
    })
  }

}
