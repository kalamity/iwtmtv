import { Component, OnInit, AfterViewInit } from '@angular/core';
import { News } from '../interfaces/news';
import { NewsService } from '../services/news.service';

@Component({
  selector: 'app-news-view',
  templateUrl: './news-view.component.html',
  styleUrls: ['./news-view.component.scss']
})
export class NewsViewComponent implements OnInit {
  private news: News[];

  constructor(private newsService: NewsService) { }

  ngOnInit() {
    this.newsService.newsSubject.subscribe(news => {
      this.news = news;
    });
    this.newsService.getNews();
  }
}
