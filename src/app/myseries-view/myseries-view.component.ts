import { Component, OnInit } from '@angular/core';
import { SeriesService } from '../services/series.service';
import { Series } from '../interfaces/series';

@Component({
  selector: 'app-myseries-view',
  templateUrl: './myseries-view.component.html',
  styleUrls: ['./myseries-view.component.scss']
})
export class MyseriesViewComponent implements OnInit {
  private series: Series[];

  constructor(private serieService: SeriesService) {}

  ngOnInit() {
    this.serieService.seriesSubject.subscribe(series => (this.series = series));
    this.serieService.getSeries();
  }
}
