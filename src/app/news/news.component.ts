import { Component, OnInit, Input, AfterViewInit } from '@angular/core';


@Component({
  selector: 'app-news',
  templateUrl: './news.component.html',
  styleUrls: ['./news.component.scss']
})
export class NewsComponent implements OnInit {
  @Input() title: string;
  @Input() media: string;
  @Input() link: string;
  @Input() text: string;

  constructor() { }

  ngOnInit() {

  }
}
