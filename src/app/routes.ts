import { Routes } from '@angular/router';
import { MyseriesViewComponent } from './myseries-view/myseries-view.component';
import { SearchComponent } from './search/search.component';
import { AuthComponent } from './auth/auth.component';
import { Error404Component } from './error404/error404.component';
import { SerieComponent } from './serie/serie.component';
import { NewsViewComponent } from './news-view/news-view.component';
import { HomeViewComponent } from './home-view/home-view.component';



export const appRoutes: Routes = [
   {path: 'myseries', component: MyseriesViewComponent},
   {path: 'news', component: NewsViewComponent},
  {path: '', component: HomeViewComponent},
  { path : 'search', component : SearchComponent},
  {path : 'auth', component : AuthComponent},
  {path : '404', component: Error404Component},
  { path: 'serie/:id', component: SerieComponent},
  /***Must stay at the end***/
  {path: '**', redirectTo: '404' }
  ]
