import { Component, OnInit } from '@angular/core';
import { SeriesService } from '../services/series.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-serie',
  templateUrl: './serie.component.html',
  styleUrls: ['./serie.component.scss']
})
export class SerieComponent implements OnInit {

  public id: string;
  public serie: any;

  constructor(private seriesService: SeriesService, private route: ActivatedRoute, private router: Router) { }

  ngOnInit() {
    this.id = this.route.snapshot.params['id'];
    this.seriesService.seriesSubject.subscribe(
      (series) => {
        this.serie = series.find(serie => serie.id === this.id)

      }
    )
  this.seriesService.getSeries()
  }


}
