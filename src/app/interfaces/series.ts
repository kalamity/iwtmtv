export interface Series {
id: string;
title: string;
network: string;
country: string;
date: Date;
season: number;
seasonV: number;
summary: string;
rating: number;
genres: Array<any>;
image: string;
}
