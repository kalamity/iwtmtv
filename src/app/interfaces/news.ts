export interface News {
    id: string;
    title: string;
    media: string;
    link: string;
    text: string;
}
