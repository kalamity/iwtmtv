import { Component, OnInit, Input } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { SearchService } from '../services/search.service';


@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit {
 private result = [];
private searchForm: FormGroup;

  constructor( private formBuilder: FormBuilder,
    private searchSerivce: SearchService
  ) {
    this.initForm();
  }
 initForm() {
   this.searchForm = this.formBuilder.group({
    search : [''],
   });
 }
  ngOnInit() {


  }

 onSubmitForm() {
   let result = this.searchForm.value ;
   this.searchSerivce.getResult(result);
 }

 

}
